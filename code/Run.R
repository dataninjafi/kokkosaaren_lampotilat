library(tidyverse)
library(xml2)


kumpula_weather_data <- function(begin_date, end_date){
  
  paivat <- seq.Date(begin_date, end_date, by = "day")
  
  ladattu <- file.path("data","kumpula-data") %>% 
    list.files(full.names = TRUE) %>% 
    lapply(readRDS) %>% 
    bind_rows() %>% 
    mutate(pvm = date(aika))
  
  paivat <- setdiff(paivat,ladattu$pvm)
  
  if(length(paivat)==0) stop()
  
  weather_query_urls(paivat) %>% 
    sapply(weather_query_save) 
}

weather_query_urls <- function(paivat){
 tibble(paiva = paivat) %>% 
    mutate(rivi = row_number(),
           jakso = rivi %% 7 %>% 
             {. == 0} %>% 
             lag %>% 
             replace_na(FALSE) %>% 
             cumsum
           ) %>% 
    group_by(jakso) %>% 
    summarise(week_min = min(paiva) %>% format("%Y-%m-%d"),
              week_max = max(paiva) %>% format("%Y-%m-%d")) %>% 
    ungroup %>% 
    transmute(url = paste0(
      "https://opendata.fmi.fi/wfs/fin?service=WFS&version=2.0.0&request=GetFeature&storedquery_id=fmi::observations::weather::timevaluepair&fmisid=101004&starttime=",
      week_min,
      "T00:00:00Z&endtime=",
      week_max,
      "T23:50:00Z&")
    ) %>% 
    pull(url)
}

weather_query_save <- function(urli){
  
  tiedosto_nimi <- urli %>% 
    str_extract_all("\\d{4}-\\d+-\\d+") %>% 
    unlist %>% 
    paste(collapse = "_") %>% 
    paste(".rds")
  
  file_path <- file.path("data",
                         "kumpula-data",
                         tiedosto_nimi)
  
  weather_query(urli) %>% 
    saveRDS(file_path)
}

weather_query <- function(urli){
  data <- read_xml(urli) %>% 
    as_list() %>% 
    as_tibble() %>%  
    unnest_longer(FeatureCollection)
  
  lampotila <- data %>% 
    slice(1) %>% 
    unnest() %>% 
    unnest(FeatureCollection) %>% 
    slice(5) %>% 
    unnest() %>% 
    unnest(FeatureCollection) %>% 
    unnest() %>% 
    unnest() %>% 
    unnest() %>% 
    setNames(c("data", "muuttuja")) %>%  
    mutate(muuttuja = rep(c("aika", "lampotila"), (nrow(.)/2)),
           id = sort(c(1:(nrow(.)/2), 1:(nrow(.)/2)))) %>% 
    pivot_wider(names_from = muuttuja,
                values_from = data) %>% 
    select(-id)
  
  tuuli_nopeus <- data %>% 
    slice(3) %>% 
    unnest() %>% 
    slice(7) %>% 
    unnest() %>% 
    unnest(FeatureCollection) %>% 
    #slice(2) %>% 
    unnest(FeatureCollection) %>% 
    unnest() %>% 
    unnest() %>% 
    unnest() %>% 
    setNames(c("data", "muuttuja")) %>%  
    mutate(muuttuja = rep(c("aika", "tuuli_nopeus"), (nrow(.)/2)),
           id = sort(c(1:(nrow(.)/2), 1:(nrow(.)/2)))) %>% 
    pivot_wider(names_from = muuttuja,
                values_from = data) %>% 
    select(-id)
  
  tuuli_suunta <- data %>% 
    slice(4) %>% 
    unnest() %>% 
    slice(7) %>% 
    unnest() %>% 
    unnest(FeatureCollection) %>% 
    #slice(2) %>% 
    unnest(FeatureCollection) %>% 
    unnest() %>% 
    unnest() %>% 
    unnest() %>% 
    setNames(c("data", "muuttuja")) %>%  
    mutate(muuttuja = rep(c("aika", "tuuli_suunta"), (nrow(.)/2)),
           id = sort(c(1:(nrow(.)/2), 1:(nrow(.)/2)))) %>% 
    pivot_wider(names_from = muuttuja,
                values_from = data) %>% 
    select(-id)
  
  rm(data)
  
  lampotila %>% 
    left_join(tuuli_nopeus) %>% 
    left_join(tuuli_suunta)
}

# "https://opendata.fmi.fi/wfs/fin?service=WFS&version=2.0.0&request=GetFeature&storedquery_id=fmi::observations::weather::timevaluepair&fmisid=101004&starttime=2019-06-01T00:00:00Z&endtime=2019-06-05T24:00:00Z&"


